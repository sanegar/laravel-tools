<?php

namespace Sanegar\Tools;

class Tools{
    /**
     * تابعی برای تبدیل اعداد انگلیسی به فارسی
     */
    public static function toPersianNumber($str){
        $persianNumbers = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];

        for($i=0; $i<count($persianNumbers); $i++){
            $str = str_replace($i, $persianNumbers[$i], $str);
        }
        return $str;
    }

    /**
     * تابعی برای تبدیل
     * \n
     *  به
     * <br>
     */
    public static function displayEnter($str = "")
    {
        // return nl2br($str);
        return str_replace(PHP_EOL,"<br>",$str);
    }
    public static function makePath($url)
    {

        $default = ['category', 'city'];
        $path=[];

        foreach($default as $var){
            if(isset($url[$var])){
                $path[] = $var.'='.$url[$var];
            }
            else if(request()->input($var) != "")
            {
                $path[] = $var.'='.request()->input($var);
            }
        }

        $queryString = implode('&', $path);

        return $queryString;
    }

    public static function getCharArray($str){
        $chars = [];
        for($i=0; $i< strlen($str); $i++){
            $chars[] = substr($str, $i, 1);
        }

        return $chars;
    }
}
