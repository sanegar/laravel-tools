<?php

namespace Sanegar\Tools;

use \Morilog\Jalali\Jalalian;

use \App;


trait ProjectTrait
{
    public function getMaxTimeEndAttribute($value)
    {
        if(App::getLocale() == "fa")
        return $this->convertNumbers(Jalalian::forge($value)->format('%A - %d %B، %Y - H:i:s'));
    else
        return Jalalian::forge($value)->format('%C-%Y');
    }

    public function getTimeEndAttribute($value)
    {
        if(App::getLocale() == "fa")
            return $this->convertNumbers(Jalalian::forge($value)->format('%A - %d %B، %Y - H:i:s'));
        else
            return Jalalian::forge($value)->format('%C-%Y');

    }

    public function getTimeOkAttribute($value)
    {
        if ($value!=null) {
            if(App::getLocale() == "fa")
                return $this->convertNumbers(Jalalian::forge($value)->format('%A - %d %B، %Y - H:i:s'));
            else
                return Jalalian::forge($value)->format('%C-%Y');
        }

    }
}
